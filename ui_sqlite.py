# -*- coding: utf-8 -*-
"""
    carmodel a user interface for the carmodel
    ~~~~~~

"""
from skimage import io
import h5py
import os
import time
from scipy import misc
from scipy.io import loadmat
import numpy as np
import json
from utils import dict_to_numpy, NumpyEncoder
import caffe
import sqlite3

import openalpr_api
from openalpr_api.rest import ApiException
from os.path import join as _j
import base64
import requests
import json
from passlib.hash import sha256_crypt

VIEW_PRED_URL = "http://18.205.67.24:8888/pred"
VIEW_HEATMAP_URL = "http://18.205.67.24:8080/damage_seg"

def callAPI(path):
    with open(path, "rb") as image_file:
        image_bytes = base64.b64encode(image_file.read())
    api_instance = openalpr_api.DefaultApi()
    secret_key = 'sk_e014dacb3b70f1a4a902b301'
    country = "us"
    recognize_vehicle = 1
    state = ''
    return_image = 0
    topn = 10
    prewarp = ''

    try:
        api_response = api_instance.recognize_bytes(image_bytes,
                                                    secret_key,
                                                    country,
                                                    recognize_vehicle=recognize_vehicle,
                                                    state='',
                                                    return_image=return_image,
                                                    topn=topn,
                                                    prewarp='')
        return api_response.to_dict()
    except ApiException as e:
        print "Exception when calling DefaultApi->recognize_bytes: %s\n" % e

#===========================DB init===========================
conn = sqlite3.connect('database.db',check_same_thread=False)
cursor = conn.cursor()
cursor.execute(""" DROP TABLE users""")
cursor.execute(""" CREATE TABLE IF NOT EXISTS users (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    name VARCHAR(30),
                    email VARCHAR(100),
                    username VARCHAR(100),
                    password VARCHAR(100)
                    )""")

# Insert default user name for demo only
cursor.execute("""INSERT INTO users (name, email, username, password) VALUES('haosheng', 'hao@liscena.com', 'haosheng',?)""",(str(sha256_crypt.encrypt('TheProject')),))
conn.commit()
cursor.close()
#===========================DB init===========================
class ModelContainer():
    def __init__(self, model_path, model_proto, label_map_path, img_shape=[224,224,3]):
        self.net = caffe.Net(model_proto, model_path, caffe.TEST)
        self.mean = (123.0,115.0,103.0)
        self.img_shape = img_shape
        self.label_map = loadmat(label_map_path)
        self.label_map = self.label_map['make_model_names']


    def forward(self, img, mean_sub=True):
        if not all([x==y for x, y in zip(img.shape, self.img_shape)]):
            img = misc.imresize(img,self.img_shape)
        if img.shape[2] == 3:
            img = img.swapaxes(0,2)
            img = img.swapaxes(1,2)
        img = img.reshape((1,) + img.shape)
        img = np.asarray(img, dtype=float)
        if mean_sub:
            img[:,:,0] = img[:,:,0] -self.mean[0]
            img[:,:,1] = img[:,:,1] -self.mean[1]
            img[:,:,2] = img[:,:,2] -self.mean[2]

        pred = self.net.forward_all(data=img)['prob']
        carmake, carmodel = self.label_map[pred.argmax()]
        return str(carmake[0]), str(carmodel[0])

    def pred_image(self, img_path):
        img = misc.imread(img_path)
        return self.forward(img)

#=======================carmodel =====================
base_path = "/data/car-model-demo"
model_path = os.path.join(base_path, "model","googlenet_finetune_web_car_iter_10000.caffemodel")
model_proto = os.path.join(base_path, "model","deploy.prototxt")
label_map_path = os.path.join(base_path, "model", "make_model_names_cls.mat")

model = ModelContainer(model_path, model_proto, label_map_path)
#=======================carmodel =====================
from flask import Flask, render_template, session, request, redirect, url_for, flash
from wtforms import Form, StringField, TextAreaField, PasswordField, BooleanField, validators
from wtforms.validators import DataRequired
from functools import wraps
from werkzeug.urls import url_parse

app = Flask(__name__)
app.config.update(dict(
    DEBUG=False,
    ENVIRONMENT= "development",
    SECRET_KEY='asdfadf development key',
    USERNAME='admin',
    PASSWORD='default'
))

# This is the pathf to the upload directory
app.config['UPLOAD_FOLDER'] = 'uploads/'
# These are the extension that we are accepting to be uploaded
app.config['ALLOWED_EXTENSIONS'] = set(['png', 'jpg', 'jpeg', 'gif'])

# For a given file, return whether it's an allowed type or not
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']

# This route will show a form to perform an AJAX request
# jQuery is loaded to execute the request and update the
# value of the operation
#@app.route('/upload_images')
#def upload_images():
#    return render_template('upload.html')

angle_to_words = {
        0 : "front",
        1 : "front-left",
        2 : "left",
        3 : "rear-left",
        4 : "rear",
        5 : "rear-right",
        6 : "right",
        7 : "front-right",
        }
# oriantation of the car
# User login
@app.route('/',methods = ['GET','POST'])
def login():
    if request.method == 'POST':
        # Get Form Fields
        username = request.form['username']
        password_candidate = request.form['password']
        cursor = conn.cursor()
        # Get user by username
        cursor.execute("""SELECT * FROM users WHERE username = ? """,(username,))
        data = cursor.fetchall()
        if len(data) > 0:
            # Get stored hash
            data = data[0]
            password = data[4] # password

            #Compare Passwords
            if sha256_crypt.verify(password_candidate, password):
                app.logger.info('PASSWORD MATCHED')
                # Passed
                session['logged_in'] = True
                session['username'] = username

                flash('You are now logged in','success')
                return render_template('demo.html')
            else:
                error = 'Invalid Password'
                app.logger.info('PASSWORD NOT MATCHED')
                return render_template('login.html', error = error)
        else:
            error = 'Username not found'
            app.logger.info('NO USER')
            return render_template('login.html', error = error)
    return render_template('login.html')

#Check if user logged in
def is_logged_in(f):
    @wraps(f)
    def wrap(*args,**kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            flash('Unauthorized, Please login', 'danger')
            return redirect(url_for('login'))
    return wrap

#Logout
@app.route('/logout')
@is_logged_in
def logout():
    session.clear()
    flash('You are now logged out','success')
    return redirect(url_for('login'))

@app.route('/demo', methods = ['GET','POST'])
@is_logged_in
def demo():
    if request.method == 'POST':
        file = request.files['file']
        # Check if the file is one of the allowed types/extensions
        if not file:
            flash("please upload a file first",'need-upload')
        if file and allowed_file(file.filename):
            #view_resp = requests.post(VIEW_PRED_URL, files={"file":file})
            print("file is received")
            view_resp = requests.post(VIEW_HEATMAP_URL, files={"file":file, "filename":file.filename})
            angle = angle_to_words[int(dict(json.loads(view_resp.text))['pred'])]
            heatmap_img_url = dict(json.loads(view_resp.text))['heatmap_url']
            heatmap_img_url = "/".join(VIEW_PRED_URL.split("/")[:-1]) + heatmap_img_url

            print("file is {}".format(file))
            # Make the filename safe, remove unsupported chars
            #filename = secure_filename(file.filename)
            filename = file.filename.lower()
            # Move the file form the temporal folder to
            # the upload folder we setup
            print(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            img = misc.imread(file)
            target_path = os.path.join(base_path, 'carmodel', 'static', filename)
            misc.imsave(target_path, img)
            openlpr_resp = callAPI(target_path)
            resp = None
            carmake, carmodel = model.forward(img)
            if len(openlpr_resp['results']) > 0:
                resp = {}
                resp['plate_number'] = openlpr_resp['results'][0]['plate']
                resp['region'] = openlpr_resp['results'][0]['region']
                if openlpr_resp['results'][0].get('vehicle', None) != None:
                    vehicle = openlpr_resp['results'][0]['vehicle']
                    resp['color'] = vehicle['color'][0]['name']
                    if vehicle['make'][0]['confidence'] > .9:
                        carmake = vehicle['make'][0]['name']
                    if vehicle['make_model'][0]['confidence'] > .9:
                        carmodel = vehicle['make_model'][0]['name']

#           flash("{} has been uploaded".format(filename))
            pred= "Make: {} \n Model: {}".format(carmake, carmodel)
            print(heatmap_img_url)
            return render_template('demo.html',
                                    pred=pred,
                                    image_path=filename,
                                    openlpr_resp=resp,
                                    heatmap_img_url=heatmap_img_url,
                                    angle=angle)
        else:
            return render_template('demo.html', wrong_file_warn="please upload an image")
    else:
        return render_template('demo.html')

class RegisterForm(Form):
    name = StringField('Name', [validators.Length(min=1, max=50)])
    username = StringField('Username', [validators.Length(min=4, max=25)])
    email = StringField('Email', [validators.Length(min=6, max=50)])
    password = PasswordField('Password', [
        validators.DataRequired(),
        validators.EqualTo('confirm', message='Passwords do not match')
    ])
    confirm = PasswordField('Confirm Password')
    remember_me = BooleanField('agree to condition',validators=[DataRequired(), ])


if __name__ == '__main__':
    app.secret_key = 'KMnsWHnaqOIUiiHJ'
    app.run(host='0.0.0.0', port=8888)
