import sqlite3
import numpy as np
import io
import os
import pdb



class SQLiteDB(object):

    """Docstring for SQLiteDB. """

    def __init__(self, db_path="sqlite.db"):
        """TODO: to be defined1. """

        sqlite3.register_adapter(np.ndarray, self._adapt_array)
        sqlite3.register_converter("array", self._convert_array)
        self.db_exist = os.path.isfile(db_path)
        self.con = sqlite3.connect(db_path,
                        detect_types=sqlite3.PARSE_DECLTYPES,
                        check_same_thread=False)
        self.cur = self.con.cursor()
        if not self.db_exist:
            self.cur.execute(self.create_record_sql())
            self.cur.execute(self.create_replay_sql())


    def create_record_sql(self):
        """
        returns sql for creating recording table
        """
        return "CREATE TABLE record (name text, \
                                     meta_data text, \
                                     arr array, \
                                     features array)"

    def create_replay_sql(self):
        """
        returns sql for creating recording table
        """
        return "CREATE TABLE record (name text, arr array)"

    def insert_record(self, name, image, meta_data=None, features=None):
        """
        inserts one image and its data during the recording
        """
        sqlite3.register_adapter(np.ndarray, self._adapt_array)
        sqlite3.register_converter("array", self._convert_array)
        if meta_data is None:
            meta_data = 'Not Available'
        if features is None:
            features = np.zeros((1))
        self.cur.execute("INSERT INTO record (name, meta_data, arr, features) values (?,?,?,?)", (name, meta_data, image, features))
        self.con.commit()

    def fetch_record(self, name, out_table=None):
        self.cur.execute("SELECT * FROM record WHERE name=?", (name,))
        if out_table is None:
            return self.cur.all()
        else:
            temp = self.cur.fetchall()
            return [v[out_table] for v in temp]

    def _adapt_array(self, arr):
        """
        defines an adapter for sqlite db.
        converts np array to text

        create the table:

        inserting data:
        cur.execute("insert into test (arr) values (?)", (x, ))
        """
        print("changing the array format")
        out = io.BytesIO()
        np.save(out, arr)
        out.seek(0)
        return sqlite3.Binary(out.read())

    def _convert_array(self, text):
        """
        reads text data from sqlite directly into np array

        reading the data
        cur.execute("select arr from test")
        data = cur.fetchone()[0]
        """
        out = io.BytesIO(text)
        out.seek(0)
        return np.load(out)

